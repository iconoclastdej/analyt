module.exports = (value, multiplyBy) => {
  return value * multiplyBy;
};
