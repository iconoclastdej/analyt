const service = require('../index.js');

describe('Scaffold functionality', () => {
  test('Should successfully multiply 6 by 7', () => {
    const serviceResult = service.serviceExample(6, 7); 
    expect(serviceResult).toBe(42);
  });
});
